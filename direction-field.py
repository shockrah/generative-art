from math import atan, cos, sin, tan

from p5 import line
from p5 import setup, draw, run
from p5 import background, size, no_fill
from p5 import stroke, stroke_weight, stroke_cap

WIDTH, HEIGHT = 750, 750
ORIGIN_X, ORIGIN_Y = WIDTH // 2,  HEIGHT // 2

grid = []

class Segment:
    def __init__(self, x:int, y:int, theta: float):
        self.x = x
        self.y = y
        self.theta = theta

        self.length = 10

    def draw(self):
        # our grid is literall 20 by 20 but we're gonna blow it up to be 200x200
        # however we're still going to maintain the same angles
        scale = 20
        offset = 150
        x = (self.x * scale) + ORIGIN_X
        y = (self.y * scale) +ORIGIN_Y

        offset_x = self.length  * cos(self.theta)
        offset_y = self.length * sin(self.theta)
        ex = x + offset_x 
        ey = y + offset_y

        line(x,y, ex,ey)

        

def y_prime(x, y):
    # NOTE: thise is where we control what we see
    base = sin(x) + cos(y)
    return tan(base * base)

def setup():
    # top left -> across then down each row like we would on paper
    # populate our grid of lines
    for x in range(-20, 20, 1):
        for y in range(20, -20, -1):
            xh = x / 2
            yh = y / 2
            grid.append(Segment(xh, yh, atan(y_prime(xh,yh))))

    no_fill()

def draw():
    background(50)
    stroke(200)
    for seg in grid:
        seg.draw()
        


if __name__ == '__main__':
    run()

