const WIDTH = window.innerWidth - 20
const HEIGHT = window.innerHeight - 20

const ORIGIN_X = WIDTH  / 2
const ORIGIN_Y = HEIGHT / 2 

let vectors = []

function y_prime(x, y) {
	const base = Math.cos(x) + Math.sin(y)
	return base * base
}

class Vector {
	constructor(x, y, angle, scale) {
		this.x = (x * scale) + ORIGIN_X
		this.y = (y * scale) + ORIGIN_Y
		this.color = Vector._interpolate_color(angle)
	}

	static _interpolate_color(m_angle) {
		// how "hot" is that vector point so we can color it appropriately
		const heat_index = (Math.abs(m_angle) % PI) / PI // basically the percentage that we're looking for

		const red = Math.floor(heat_index * 0xff)
		const blue = Math.floor(0xff - red)

		return [red, 0, blue, heat_index * 0xff]
	}

	draw() {
		const [r, g, b, a] = [this.color[0], this.color[1], this.color[2], this.color[3]]
		stroke(r, g, b, a)
		point(this.x, this.y)
	}
}

class Velocity {
	constructor() {
		const max = 2

		this.theta = Math.random() * Math.PI * 2
		this.mag = Math.random() * max
		while(this.mag < 0.2 * max) {
			this.mag = Math.random() * max
		}
	}
}

class Ball {
	constructor(x, y, vel) {
		this.x = x
		this.y = y 
		this.vel = vel
	}

	edges() {
		if(this.x > WIDTH) { this.x = 0 }
		if(this.x < 0) { this.x = WIDTH }

		if(this.y > HEIGHT) { this.y = 0 }
		if(this.y < 0) { this.y = HEIGHT }
	}

	move() {
		this.vel.theta = y_prime(this.x, this.y)

		const x_move = this.vel.mag * Math.cos(this.vel.theta)
		const y_move = this.vel.mag * Math.sin(this.vel.theta)
		this.x += x_move
		this.y += y_move

		this.edges()
	}

	draw() {
		point(this.x, this.y)
	}
}


let ballpit = []
function setup() {
	createCanvas(WIDTH, HEIGHT)
	const lb = -10
	const up = 10
	//for(var i = 0;  i<500; i++) {
	//	const x = (Math.random() * WIDTH)
	//	const y = (Math.random() * HEIGHT)
	//	const vel = new Velocity()

	//	ballpit.push(new Ball(x, y, vel))
	//}
	for(var x = lb;x < up;x++) {
		for(var y = lb; y < up; y++) {
			let p = new Vector(x, y, y_prime(x,y), 15)
			vectors.push(p)
		}
	}

	noFill()
}

function draw() {
	background(210, 180, 140) // tan background
	strokeWeight(10)
	for(const sample of vectors) {
		sample.draw()
	}

	// ballpit things
	//textSize(40)
	//text(getFrameRate(), 100, 100)
	//for(const ball of ballpit) {
	//	ball.draw()
	//	ball.move()
	//}
}
