class PVec {
	constructor(x,y) {
		this.x = x
		this.y = y
	}

	static dist(r1, r2)  {
		const left = (r2.x - r1.x) * (r2.x - r1.x)
		const right = (r2.y - r1.y) * (r2.y - r1.y)

		return Math.sqrt(left + right)
	}

	magnitude() {
		return PVec.dist(this, new PVec(0,0))
	}
}

class Hyperbola {
	constructor(f1, f2) {
		// Gives back all the relevant details for a hyperbola where point p lies on a hyperbola whose foci are f1, f2
		this.h = (f1.x + f2.x) / 2
		this.k = (f1.y + f2.y) / 2
		this.f1 = f1
		this.f2 = f2


		// /making-shit-up
		const center = new PVec(this.h,this.k)
		const focal_distance = PVec.dist(center, f1)

		this.major_axis = focal_distance * 2 / 3;
		this.minor_axis = this.major_axis  // get a nice square with some cheesey angles

		this.asym_up 	= 1  // refers to this one /
		this.asym_down 	= -1 // refers to this one \

		this.ecc =   Math.sqrt(Math.pow(this.major_axis,2) + Math.pow(this.minor_axis, 2)) / this.major_axis
	}

	point_relative(x,y) {
		// (x - h)^2 / a^2 - (y - k) ^2 / b^2 - 1
		const left = Math.pow(x-this.h,2) / Math.pow(this.major_axis, 2)
		const rght = Math.pow(y-this.k,2) / Math.pow(this.minor_axis, 2)

		return left - rght - 1
	}

	close() {
		// cut in half each time we want to close
		this.ecc = this.ecc + 1 / 2 
	}
	open() {
		this.ecc = this.ecc * 1.5
	}
}

function main() {
	let pad = document.getElementById('tester')

	const r1 = new PVec(1,5)
	const r2 = new PVec(5, 5)
	const attr = new PVec(3,15)

	const d = PVec.dist(r1, r2) / 2

	let A = 0;
	const attr_r1 = r1.magnitude() - A
	const attr_r2 = r2.magnitude() + A

	const h = new Hyperbola(r1, r2)
	console.log('Just the eccentricity', h.ecc);
	//console.log('in the mouth', h.point_relative(0,5), h.point_relative(6,5))
	//console.log('not in mouth', h.point_relative(3,16))

	console.log('Before increment', h.ecc)
	h.close()
	console.log('After increment', h.ecc)


	Plotly.newPlot( 
		pad, 
		[{
			x: [0, 1, 3, 5],
			y: [0, 5, 15, 5],
			type: 'scatter'
		}], 
		{
			margin: { t: 0 },
			showline: false
		}
	);
}
