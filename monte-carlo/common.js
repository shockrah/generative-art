
const WIDTH = window.innerWidth - 20
const HEIGHT = window.innerHeight - 20

const ORIGIN_X = WIDTH  / 2
const ORIGIN_Y = HEIGHT / 2 

const SAMPLES = 200 // number of test points to generate

let RUNNING_AVERAGE = null
let AREA_SUM = null
let ROUNDS = null

let curve_set
let monte_carlo_points


class Point {
    constructor(x,y){
        this.x = x + ORIGIN_X
        this.y = y + ORIGIN_Y
    }
}

/**
 * 
 * @param {Point} point_tl 
 * @param {Point} point_br 
 * @param {Number} samples
 */
function generate_test_points(point_tl, point_br, samples) {
    // rectangluar area Given by two points points are generated in this area
    const x_min = (point_tl.x <= point_br.x) ? point_tl.x : point_br.x
    const y_min = (point_tl.y <= point_br.x) ? point_tl.y : point_br.y

    const x_max = (point_tl.x > point_br.x)  ? point_tl.x : point_br.x
    const y_max = (point_tl.y > point_br.y)  ? point_tl.y : point_br.y

    let set = []
    for(var i = 0; i < samples; i++) {
        const x = (x_max - x_min) * Math.random() + x_min
        const y = (y_max - y_min) * Math.random() + y_min
        set.push(new Point(x-ORIGIN_X, y-ORIGIN_Y))
    }
    return set
}