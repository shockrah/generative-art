const TOPLEFT = new Point(-250, -20)
const BOTTOMRIGHT = new Point(250,20)

function my_curve(x) {
    return 20 * Math.sin(x/20)
}

function generator(x_start, x_end, step, func) {
    // generates some points along a given curve function
    let set = []
    for(var i = x_start; i<x_end; i += step) {
        const y = func(i)
        set.push(new Point(i, y))
    }
    return set
}


function setup_sine_sim() {
    createCanvas(WIDTH, HEIGHT)
    curve_set = generator(-250, 250, 5, my_curve)
    monte_carlo_points = generate_test_points(TOPLEFT, BOTTOMRIGHT, 200)
}

function sine_wave_sim() {
    background(210, 180, 140) // tan ish
    strokeWeight(5)
    
    textAlign(CENTER)

    VALID_COUNT = 0 
    monte_carlo_points = generate_test_points(TOPLEFT, BOTTOMRIGHT, 200)
    for(const p of monte_carlo_points) {
        // f(x) - y < 0 above
        // f(x) - y > 0 below
        // f(x) - y = 0 on the curve
        const f_x = my_curve(p.x - ORIGIN_X)
        const y = p.y - ORIGIN_Y // undo the offset
        stroke('green')
        if(f_x - y <= 0) {
            VALID_COUNT += 1
            point(p.x, p.y)
        }
    }

    // drawing our curve
    stroke('black')
    for(const p of curve_set) {
        point(p.x, p.y)
    }

    stroke('white')
    point(ORIGIN_X, ORIGIN_Y)

    strokeWeight(0)
    const area = (VALID_COUNT / SAMPLES) *  (500 * 40)/* area of rectangle */
    if(RUNNING_AVERAGE == null) {
        RUNNING_AVERAGE = area
        AREA_SUM = area
        ROUNDS = 1
    }
    else {
        AREA_SUM += area
        ROUNDS++
        RUNNING_AVERAGE = AREA_SUM / ROUNDS
    }
    text(`Test Points: ${monte_carlo_points.length}`, ORIGIN_X, ORIGIN_Y - 120)
    text(`Average area: ${Math.floor(RUNNING_AVERAGE)}`, ORIGIN_X, ORIGIN_Y - 100)
    text(`Max area: ${500 * 40}`, ORIGIN_X, ORIGIN_Y - 80)
}

function convex_sim() {
    // monte carlo integration of an arbitrary convex shape
}