const WIDTH = window.innerWidth - 20
const HEIGHT = window.innerHeight - 20

const ORIGIN_X = WIDTH  / 2
const ORIGIN_Y = HEIGHT / 2 

function setup() {
    createCanvas(WIDTH, HEIGHT)
    noLoop()
}


function draw() {
    const limit = 100
    noStroke()
    background(210, 180, 140) // tan ish

    fill(148, 49, 38, 75) // red ish
    triangle(ORIGIN_X, ORIGIN_Y - 150, 0, HEIGHT, WIDTH, HEIGHT) // rays

	// casting out a bunch of rays w/ some triangles yo
	
    fill(210, 180, 140, 255) //  tan ish
    circle(ORIGIN_X,ORIGIN_Y - 100, 2 * limit + 10) // blocker


	// Sun things
    fill(148, 49, 38, 150) // red ish
    circle(ORIGIN_X,ORIGIN_Y - 100, 2 * limit) // sun

    stroke(72, 60, 50) // black for the particles
    for(let angle = 0.0001; angle < Math.PI * 2; angle += Math.PI/64) {
        for(let i = 0;i<limit;i++) {
            let y = ORIGIN_Y + i * Math.sin(angle) - 100
            let x = ORIGIN_X + i * Math.cos(angle)
            if(Math.random() > i/limit) {
                point(x,y)
            }
        }
    }
    
}
