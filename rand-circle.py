from random import randint, uniform
from math import pi
from p5 import circle, arc, rotate, millis
from p5 import setup, draw, run
from p5 import background, size, no_fill
from p5 import stroke, stroke_weight, stroke_cap

WIDTH, HEIGHT = 750, 750
CENTER_X, CENTER_Y = WIDTH // 2,  HEIGHT // 2

class ArcData:
    def __init__(self, x, y, r, start, stop):
        self.x = x
        self.y = y
        self.r = r
        self.start = start
        self.stop = stop
        self.spin = millis() / 1000 * uniform(-1., 1.)
        self.color = (randint(0, 255),randint(0, 255),randint(0, 255))

arcs = []

def setup():
    size(WIDTH, HEIGHT)
    stroke(200)
    stroke_weight(7)
    stroke_cap('ROUND')
    no_fill()
    for i in range(15):
        start = randint(20, 30)
        stop = randint(50, 80)

        start = (start / 100) * 2 * pi
        stop = (stop / 100) * 2 * pi
        r = 100 + (30 * (i + 1))

        arcs.append(ArcData(CENTER_X, CENTER_Y, r, start, stop))

def draw_arcs():
    for idx, a in enumerate(arcs):
        stroke(*a.color)
        try: 
            arc(a.x, a.y, a.r, a.r, a.start, a.stop)
        # this is here incase any arcs are setup to be too small
        except AssertionError:
            a.start = .3 * 2 * pi
            a.stop =  .5 * 2 * pi
            arc(a.x, a.y, a.r, a.r, a.start, a.stop)

def draw():
    global arcs
    background(50) # black-ish background
    # first we addjust the start/ stop angles by whatever scale they have
    for idx, a in enumerate(arcs):
        arcs[idx].start += arcs[idx].spin
        arcs[idx].stop += arcs[idx].spin

    draw_arcs()
        

if __name__ == '__main__':
    run()
