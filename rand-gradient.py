import OpenGL as opengl

from  OpenGL.GL import *
from  OpenGL.GLUT import *
from  OpenGL.GLU import *

import random
WIDTH, HEIGHT = 1000, 1000

SCAN_LINES = None

GRADIENT = None

def iterate():
    glViewport(0,0, WIDTH, HEIGHT)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glOrtho(0.0, 500, 0.0, 500, 0.0, 1.0)
    glMatrixMode (GL_MODELVIEW)
    glLoadIdentity()


def gradient():
    global GRADIENT
    if GRADIENT is None:
        GRADIENT = []
        for i in range(WIDTH):
            GRADIENT.append([0, .5 - i/WIDTH, 0.5 - i/WIDTH])

    for idx, step in enumerate(GRADIENT):
        glColor3f(step[0], step[1], step[2])
        glBegin(GL_QUADS)

        bottom_left = idx
        glVertex2f(bottom_left, 0) # bot left
        glVertex2f(bottom_left, HEIGHT) # top left 
        glVertex2f(bottom_left + 1, HEIGHT) # top right
        glVertex2f(bottom_left + 1, 0) # bot right

        glEnd()


def scanlines():
    # draw a line from the top to the bottom
    from random import randint
    rand_val = lambda: randint(0,100)/100
    global lines
    if lines is None:
        lines = []
        for red in range(10, 0, -1): # in terms of %
            for green in range(10, 0, -1):
                for blue in range(10, 0, -1):
                    lines.append([red/10, blue/10, green/10])
                    #lines.append([rand_val(), rand_val(), rand_val()])

    # once we have our colors we need to know the windows width and partition the proper amount of space for each
    # partitioning the horizontal space first
    lwidth = 1
    for step, line in enumerate(lines):
        glColor3f(line[0], line[1], line[2])
        glBegin(GL_QUADS)

        bottom_left = step * lwidth
        glVertex2f(bottom_left, 0) # bot left
        glVertex2f(bottom_left, HEIGHT) # top left 
        glVertex2f(bottom_left + lwidth, HEIGHT) # top right
        glVertex2f(bottom_left + lwidth, 0) # bot right

        glEnd()



def rect():
    glBegin(GL_QUADS)
    glVertex2f(100, 100) # bot left
    glVertex2f(100, 200) # top left 
    glVertex2f(200, 200) # top right
    glVertex2f(200, 100) # bot right
    glEnd()


def show_screen():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity() # reset all entity positions
    iterate()
    glColor3f(1.0, 1.0, 0.0)
    # NOTE: here are some sample functions that random things
    #rect()
    #scanlines()
    gradient()
    glutSwapBuffers()



glutInit()
glutInitDisplayMode(GLUT_RGBA)
glutInitWindowSize(WIDTH, HEIGHT)
glutInitWindowPosition(100,100)

window = glutCreateWindow('first opengl lesson')
glutDisplayFunc(show_screen)
glutIdleFunc(show_screen)
glutMainLoop()
