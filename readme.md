# Generative Art

From Wikipedia: "Generative art" often refers to algorithmic art (algorithmically determined computer generated artwork) and synthetic media (general term for any algorithmically-generated media), but artists can also make it using systems of chemistry, biology, mechanics and robotics, smart materials, manual randomization, mathematics, data mapping, symmetry, tiling, and more.

Mostly written with javascript, python and p5, however some are written with opengl. All of these are written for pure enjoyment of the abstract.

## scuffed/

Contains projects/idea/experiments that might work but are generally not really much of anything.

## screen-caps/

Contains images/video of things doing stuff.
